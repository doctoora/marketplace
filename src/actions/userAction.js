import axios from "axios";
import { GET_USER, GET_ERRORS } from "./types";
import setAuthTokenAndBaseURL from "../utils/setAuthToken";

export const getUser = user => dispatch => {
  dispatch({
    type: GET_USER,
    payload: user
  });
};

export const loginAdminUser = data => dispatch => {
  axios
    .post("/adminuser/login", data)
    .then(res => {
      localStorage.setItem("token", res.data.token);
      setAuthTokenAndBaseURL(res.data.token);
      dispatch({
        type: GET_USER,
        payload: res.data
      });
    })
    .catch(e => {
      dispatch({
        type: GET_ERRORS,
        payload: "Your credentials are not correct"
      });
    });
};
export const loginDoctor = data => dispatch => {
  axios
    .post("/doctoruser/login", data)
    .then(res => {
      localStorage.setItem("token", res.data.token);
      setAuthTokenAndBaseURL(res.data.token);
      dispatch({
        type: GET_USER,
        payload: res.data
      });
    })
    .catch(e => {
      dispatch({
        type: GET_ERRORS,
        payload: "Your credentials are not correct"
      });
    });
};
export const loginUser = data => dispatch => {
  axios
    .post("/patientuser/login", data)
    .then(res => {
      localStorage.setItem("token", res.data.token);
      setAuthTokenAndBaseURL(res.data.token);
      dispatch({
        type: GET_USER,
        payload: res.data
      });
    })
    .catch(e => {
      dispatch({
        type: GET_ERRORS,
        payload: "Your credentials are not correct"
      });
    });
};
