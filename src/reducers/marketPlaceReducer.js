import { GET_HMO } from "../actions/types";

const initialState = {
  hmo: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_HMO:
      return {
        ...state,
        hmo: action.payload
      };
    default:
      return state;
  }
}
