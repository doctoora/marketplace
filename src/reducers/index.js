import { combineReducers } from "redux";

import errorReducer from "./errorReducer";
import marketPlaceReducer from "./marketPlaceReducer";
import userReducer from "./userReducer";

export default combineReducers({
  errors: errorReducer,
  hmo: marketPlaceReducer,
  user: userReducer
});
