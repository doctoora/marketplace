import axios from "axios";

const setAuthTokenAndBaseURL = token => {
  axios.defaults.baseURL = "https://server.doctoora.com";
  if (token) {
    axios.defaults.headers.common["Authorization"] = "Bearer " + token;
  } else {
    delete axios.defaults.headers.common["Authorization"];
  }
};

export default setAuthTokenAndBaseURL;
