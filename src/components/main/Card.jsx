import React, { Component } from "react";

export default class Card extends Component {
  render() {
    const { name, address, _id } = this.props.data;
    const click = this.props.click;
    return (
      <div className="col-md-4">
        <div className="panel" style={{ borderRadius: 20 + "px" }}>
          <div className="miniStats--panel">
            <div className="miniStats--header bg-p">
              <img
                src="assets/img/dashboard.png"
                style={{ width: "auto", height: "auto" }}
              />
            </div>

            <div className="miniStats--body panel-content pt-12">
              <h5 className="miniStats--title">{name}</h5>

              <div className="form-group row">
                <div className="col-md-5">
                  <p className="miniStats--caption">
                    <i className="fa fa-calendar" /> 08/08/ 2019
                  </p>
                </div>
                <div className="col-md-4">
                  <p className="miniStats--caption">
                    <i className="fa fa-clock" /> 6pm
                  </p>
                </div>
                <div className="col-md-12">
                  <p className="miniStats--caption">
                    <i className="fa fa-map-marker" /> {address}
                  </p>
                </div>
              </div>
              <div className="col-md-12">
                <a
                  href="#vCenteredModals"
                  className="btn btn-info"
                  style={{ width: "100%" }}
                  data-toggle="modal"
                  onClick={() => click(_id)}
                >
                  Explore
                </a>
              </div>
            </div>
          </div>
        </div>

        <div id="vCenteredModals" className="modal fade">
          <div className="modal-dialog modal-dialog-centered">
            <div className="modal-content">
              <div className="modal-header">
                <img
                  src="assets/img/dashboard.png"
                  style={{ width: "auto", height: "auto" }}
                />
              </div>

              <div className="modal-body">
                <h5 className="modal-title" style={{ textAlign: "center" }}>
                  {}
                </h5>
                <div className="miniStats--body panel-content pt-12">
                  <div className="form-group row">
                    <div className="col-md-4">
                      <p className="miniStats--caption">
                        <i className="fa fa-calendar" /> 08/08/2019
                      </p>
                    </div>
                    <div className="col-md-4">
                      <p className="miniStats--caption">
                        <i className="fa fa-clock" /> 6pm
                      </p>
                    </div>
                    <div className="col-md-4">
                      <p className="miniStats--caption">
                        <i className="fa fa-clock" /> #3000
                      </p>
                    </div>
                    <div className="col-md-12">
                      <p className="miniStats--caption">
                        <i className="fa fa-map-marker" />
                        {}
                      </p>
                    </div>
                  </div>
                  This workout is focused on helping you achieve blah blah. The
                  quick, brown fox jumps over a lazy dog. DJs flock by when MTV
                  ax quiz progra.
                </div>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-default"
                  data-dismiss="modal"
                >
                  Close
                </button>

                <button type="button" className="btn btn-success">
                  Register
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
