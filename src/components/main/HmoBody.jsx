import React, { Component } from "react";
import Card from "./Card";
import Modal from "./Modal";
import { connect } from "react-redux";
import axios from "axios";
import { Route, Link, Switch } from "react-router-dom";
import Navbar from "../Navbar";
import SidebarNav from "../SidebarNav";
import Main from "../Main";
// import Test from "./Test";

import { getHmo } from "../../actions/marketPlaceActions";

class HmoBody extends Component {
  constructor(props) {
    super(props);
    this.state = {
      everything: [],
      loaded: false,
      each: {}
    };
    this.handleClick = this.handleClick.bind(this);
  }
  componentWillMount() {
    axios.get("/hmo").then(res => {
      this.setState({
        everything: res.data
      });
    });
  }
  handleClick(id) {
    console.log(id);
    const { everything } = this.state;
    const singleCard = everything.filter(every => every._id === id);
    this.setState({
      each: singleCard
    });
  }
  render() {
    const { everything } = this.state;
    return (
      <div>
        <Navbar />
        <SidebarNav />
        <Main />
        <div className="tab-content">
          <div className="row">
            <div className="" />
            <div className="">
              <div className="tab-pane fade show active" id="tab07">
                <div className="row gutter-20">
                  {everything &&
                    everything.map(every => {
                      return (
                        <Card
                          data={every}
                          click={this.handleClick}
                          key={every._id}
                        />
                      );
                    })}{" "}
                  {/* <Modal data={this.state.each} /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hmo: state.hmo
});

export default connect(
  mapStateToProps,
  { getHmo }
)(HmoBody);
