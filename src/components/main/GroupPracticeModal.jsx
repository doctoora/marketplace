import React, { Component } from "react";

class Modal extends Component {
  render() {
    // const { address, name } = this.props.data[0];
    console.log();
    return (
      <div id="vCenteredModal" className="modal fade">
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-header">
              <img
                src="assets/img/dashboard.png"
                style={{ width: "auto", height: "auto" }}
              />
            </div>

            <div className="modal-body">
              <h5 className="modal-title" style={{ textAlign: "center" }}>
                {}
              </h5>
              <div className="miniStats--body panel-content pt-12">
                <div className="form-group row">
                  <div className="col-md-4">
                    <p className="miniStats--caption">
                      <i className="fa fa-calendar" /> 08/08/2019
                    </p>
                  </div>
                  <div className="col-md-4">
                    <p className="miniStats--caption">
                      <i className="fa fa-clock" /> 6pm
                    </p>
                  </div>
                  <div className="col-md-4">
                    <p className="miniStats--caption">
                      <i className="fa fa-clock" /> #3000
                    </p>
                  </div>
                  <div className="col-md-12">
                    <p className="miniStats--caption">
                      <i className="fa fa-map-marker" />
                      {}
                    </p>
                  </div>
                </div>
                This workout is focused on helping you achieve blah blah. The
                quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax
                quiz prog.
              </div>
            </div>

            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default"
                data-dismiss="modal"
              >
                Close
              </button>

              <button type="button" className="btn btn-success">
                Register
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
