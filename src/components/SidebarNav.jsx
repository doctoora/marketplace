import React, { Component } from "react";
import { connect } from "react-redux";

import { getAll } from "../actions/marketPlaceActions";

class SidebarNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }
  componentDidMount() {
    this.setState({
      user: this.props.user.user
    });
  }
  render() {
    const user = {
      email: this.props.user.user.email,
      firstname: this.props.user.user.userFName
        ? this.props.user.user.userFName
        : this.props.user.user.doctorFName,
      lastname: this.props.user.user.doctorLName
        ? this.props.user.user.doctorLName
        : this.props.user.user.userLName
    };

    return (
      <aside className="sidebar" data-trigger="scrollbar">
        <div className="sidebar--profile">
          <div className="profile--img">
            <a href="profile.html">
              <img
                src="assets/img/avatars/01_80x80.png"
                alt=""
                className="rounded-circle"
              />
            </a>
          </div>

          <div className="profile--name">
            <a className="btn-link">Hello</a>
            <p> {user.firstname + " " + user.lastname}</p>
          </div>
        </div>

        <div className="sidebar--nav">
          <ul>
            <li>
              {/* <ul>
                <li className="active">
                  <a href="index.html">
                    <i className="fa fa-user" />
                    <span>Profile</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-briefcase" />
                    <span>Professional</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-folder" />
                    <span>Health Record</span>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <i className="fa fa-history" />
                    <span>History</span>
                  </a>
                </li>
              </ul> */}
            </li>
          </ul>
        </div>
      </aside>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(SidebarNav);
