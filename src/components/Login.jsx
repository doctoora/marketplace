import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { loginUser, loginDoctor, loginAdminUser } from "../actions/userAction";
import TextFieldGroup from "./TextFieldGroup";
import { ClipLoader } from "react-spinners";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
      userType: "",
      formDisabled: true,
      loading: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }

  componentDidMount() {
    console.log(this.props);
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }

    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors, loading: false });
    }
  }

  onSelect(e) {
    console.log("selection changed");
    this.setState({ userType: e.target.value, formDisabled: false });
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({ loading: true });

    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    switch (this.state.userType) {
      case "doctor":
        this.props.loginDoctor(userData);
        break;
      case "adminUser":
        this.props.loginAdminUser(userData);
        break;
      case "user":
        this.props.loginUser(userData);
        break;
      default:
        break;
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.state;

    return (
      <div className="login">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Log In</h1>
              <p className="lead text-center">
                Sign in to your doctoora account
              </p>
              <ClipLoader
                // css={override}
                sizeUnit={"px"}
                size={50}
                color={"#123abc"}
                loading={this.state.loading}
              />
              <form onSubmit={this.onSubmit}>
                <p>What type of user are you?</p>
                <select className="form-control mb-5" onChange={this.onSelect}>
                  <option value="">Select</option>
                  <option value="doctor">Doctor</option>
                  <option value="adminUser">Admin User</option>
                  <option value="user">User</option>
                </select>
                <TextFieldGroup
                  placeholder="Email Address"
                  name="email"
                  type="email"
                  value={this.state.email}
                  onChange={this.onChange}
                  error={errors.email}
                  disabled={this.state.formDisabled}
                />

                <TextFieldGroup
                  placeholder="Password"
                  name="password"
                  type="password"
                  value={this.state.password}
                  onChange={this.onChange}
                  error={errors.error}
                  disabled={this.state.formDisabled}
                />
                <input
                  type="submit"
                  disabled={this.state.formDisabled}
                  className="btn btn-info btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.user,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser, loginAdminUser, loginDoctor }
)(Login);
