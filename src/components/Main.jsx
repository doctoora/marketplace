import React, { Component } from "react";
import Card from "./main/Card";
import Modal from "./main/Modal";
import { connect } from "react-redux";
import axios from "axios";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import SidebarNav from "./SidebarNav";
import { ClipLoader } from "react-spinners";
// import Navbar from "./Navbar";

import { getHmo } from "../actions/marketPlaceActions";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      everything: [],
      loaded: false,
      each: {},
      showModal: false,
      loading: true
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    axios.get("/hmo").then(res => {
      this.setState({
        everything: res.data
      });
    });
    this.setState({ loading: false });
  }

  handleClick(id) {
    console.log(id);
    const { everything } = this.state;
    const singleCard = everything.filter(every => every._id === id);
    this.setState({
      each: singleCard
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      everything: nextProps.products
    });
  }

  render() {
    const { everything } = this.state;
    return (
      <div>
        <Navbar />
        <SidebarNav />
        <main className="main--container">
          <section className="page--header">
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-12">
                  <h2 className="page--title h5">Find a group practice</h2>
                  <div className="panel-content pt-12">
                    <div className="form-group row">
                      <div className="col-md-3">
                        <select name="select" className="form-control">
                          <option value="1">Select Specialization</option>
                          <option value="2">Option 2</option>
                          <option value="3">Option 3</option>
                          <option value="4">Option 4</option>
                        </select>
                      </div>

                      <div className="col-md-3">
                        <select name="select" className="form-control">
                          <option value="1">Select Specialization</option>
                          <option value="2">Option 2</option>
                          <option value="3">Option 3</option>
                          <option value="4">Option 4</option>
                        </select>
                      </div>

                      <div className="col-md-3">
                        <input name="date" type="date" className="form-control">
                          {/* <option value="1">DD-MM-YY</option>
                                          <option value="2">Option 2</option>
                                          <option value="3">Option 3</option>
                                          <option value="4">Option 4</option> */}
                        </input>
                      </div>

                      <div className="col-md-3">
                        {/* <div class="action">
                                        <a href="#" class="btn btn-info" style="width: 100%">Search</a>
                                    </div> 
                                      </select>*/}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>

          <section className="main--content">
            <ul className="nav nav-tabs nav-tabs-line">
              <li className="nav-item">
                {/* <a href="#tab07" data-toggle="tab" className="nav-link active">
                Group Practice
              </a> */}
                <Link to="/group-practice" className="nav-link">
                  {" "}
                  Group Practice{" "}
                </Link>
              </li>
              <li className="nav-item">
                {/* <a href="#tab08" data-toggle="tab" className="nav-link">
                Health Plans
              </a> */}
                <Link to="/" className="nav-link">
                  {" "}
                  HMO{" "}
                </Link>
              </li>
              {/* <li className="nav-item">
              <a href="#tab09" data-toggle="tab" className="nav-link">
                Healthcare Products
              </a>
            </li>
            <li className="nav-item">
              <a href="#tab09" data-toggle="tab" className="nav-link">
                Events
              </a>
            </li> */}
            </ul>

            <div className="tab-content">
              <ClipLoader
                // css={override}
                sizeUnit={"px"}
                size={50}
                color={"#123abc"}
                loading={this.state.loading}
              />
              <div className="tab-pane fade show active" id="tab07">
                <div className="row gutter-20">
                  {everything &&
                    everything.map(every => {
                      return (
                        <Card
                          data={every}
                          key={every.id}
                          click={this.handleClick}
                        />
                      );
                    })}

                  <Modal data={this.state.each} />
                </div>
              </div>
            </div>

            <div id="vCenteredModal" className="modal fade">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-header">
                    <img
                      src="assets/img/dashboard.png"
                      style={{ width: "auto", height: "auto" }}
                    />
                  </div>

                  <div className="modal-body">
                    <h5 className="modal-title" style={{ textAlign: "center" }}>
                      Mental Health Masterclass
                    </h5>
                    <div className="miniStats--body panel-content pt-12">
                      <div className="form-group row">
                        <div className="col-md-5">
                          <p className="miniStats--caption">
                            <i className="fa fa-calendar" /> 08/08/ 2019
                          </p>
                        </div>
                        <div className="col-md-4">
                          <p className="miniStats--caption">
                            <i className="fa fa-clock" /> 6pm
                          </p>
                        </div>
                        <div className="col-md-12">
                          <p className="miniStats--caption">
                            <i className="fa fa-map-marker" /> 20A Gbajumo
                            Crescent Surulere Lagos
                          </p>
                        </div>
                      </div>
                      This workout is focused on helping you achieve blah blah.
                      The quick, brown fox jumps over a lazy dog. DJs flock by
                      when MTV ax quiz prog.
                    </div>
                  </div>

                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-default"
                      data-dismiss="modal"
                    >
                      Close
                    </button>

                    <button type="button" className="btn btn-success">
                      Register
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  hmo: state.hmo
});

export default connect(
  mapStateToProps,
  { getHmo }
)(Main);
