import React from "react";
import "./App.css";
import Navbar from "./components/Navbar";
import SidebarNav from "./components/SidebarNav";
import Main from "./components/Main";
import Login from "./components/Login";

import HmoBody from "./components/main/HmoBody";
import GroupPracticeBody from "./components/GroupPracticeBody";
import setAuthTokenAndBaseURL from "./utils/setAuthToken";
import jwt_decode from "jwt-decode";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import store from "./store";
import { Provider } from "react-redux";
import PrivateRoute from "./components/PrivateRoute";

import { getUser } from "./actions/userAction";

try {
  setAuthTokenAndBaseURL(null);
  if (localStorage.token) {
    const token = localStorage.getItem("token");
    setAuthTokenAndBaseURL(token);

    store.dispatch(getUser(jwt_decode(token)));

    const decoded = jwt_decode(localStorage.token);

    const currentTime = Date.now() / 1000;

    if (decoded.exp < currentTime) {
      window.location.href("/login");
      localStorage.setItem("token", "");
      localStorage.clear();
      // store.dispatch(logoutUser());
      // store.dispatch(clearCurrentProfile());
      // this.props.history.push("/login");
    }
  }
  //   console.log("token eists");
} catch (error) {
  // invalid token format
}

function App() {
  return (
    <Provider store={store}>
      <Router>
        {/* <Navbar />
        <SidebarNav />
        <Main /> */}
        <div className="App">
          <Switch>
            <Route exact path="/login" component={Login} />
          </Switch>
          <Switch>
            <PrivateRoute exact path="/" component={Main} />
            {/* <PrivateRoute exact path="/hmo" component={HmoBody} /> */}
            <PrivateRoute
              exact
              path="/group-practice"
              component={GroupPracticeBody}
            />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
